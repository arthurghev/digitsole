package com.example.chartsapp.presentation.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.example.chartsapp.R
import com.example.chartsapp.common.AppPreferences
import com.example.chartsapp.common.utilities.Status
import com.example.chartsapp.databinding.FragmentLoginBinding
import com.example.chartsapp.presentation.Model.UserModel
import com.example.chartsapp.presentation.registration.RegistrationViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {

    private val viewModel: LoginFragmentViewModel by viewModel()
    private lateinit var binding: FragmentLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObservers()
        binding.btnRegistration.setOnClickListener(View.OnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registration)
        })
        binding.btnLogin.setOnClickListener(View.OnClickListener {
            viewModel.getUserByEmail(binding.editTextEmailAddress.toString())
        })
    }

    private fun initObservers() {
        viewModel.userEntity.observe(viewLifecycleOwner, Observer {
            when (it.status){
                Status.SUCCESS -> {
                    //progressBar.visibility = View.GONE
                    AppPreferences.setToken("token")
                    findNavController().navigate(R.id.action_registration_to_myPageFragment)
                }
                Status.LOADING -> {
                    //progressBar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    //progressBar.visibility = View.GONE
                    Toast.makeText(activity, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}