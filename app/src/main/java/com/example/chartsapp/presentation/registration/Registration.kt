package com.example.chartsapp.presentation.registration

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.chartsapp.R
import com.example.chartsapp.common.AppPreferences
import com.example.chartsapp.data.local.UserEntity
import com.example.chartsapp.databinding.FragmentRegistrationBinding
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

class Registration : Fragment() {
    private val viewModel: RegistrationViewModel by viewModel()
    private var cal = Calendar.getInstance()
    private lateinit var binding: FragmentRegistrationBinding

    companion object {
        fun newInstance() = Registration()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegistrationBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initDatePickerView()
        initGenderView()
        setListener()
    }


    private fun setListener() {
        binding.btnSave.setOnClickListener(View.OnClickListener {
            if (checkUserData()) {
                lifecycleScope.launch {
                    viewModel.insertUserInDb(getUserData())
                    Toast.makeText(activity, "Registration completed successfully",
                        Toast.LENGTH_SHORT
                    ).show()
                    AppPreferences.setToken("token")
                    findNavController().navigate(R.id.action_registration_to_myPageFragment)
                }
            }
        })

        binding.btnCancel.setOnClickListener(View.OnClickListener {
            clearFilds()
        })
    }

    private fun clearFilds() {
        binding.editTextFullName.text.clear()
        binding.editTextEmailAddress.text.clear()
        binding.editTextDate.text.clear()
        binding.editTextGender.text.clear()
        binding.editTextPassword.text.clear()
        binding.editTextConfirmPassword.text.clear()
    }

    private fun checkUserData(): Boolean {
        if (binding.editTextFullName.text.toString().isEmpty() ||
            binding.editTextEmailAddress.text.toString().isEmpty() ||
            binding.editTextDate.text.toString().isEmpty() ||
            binding.editTextPassword.text.toString().isEmpty() ||
            binding.editTextGender.text.toString().isEmpty() ||
            binding.editTextConfirmPassword.text.toString().isEmpty()
        ) {
            Toast.makeText(activity, getString(R.string.registration_fill_in_all_fields), Toast.LENGTH_LONG).show()
            return false
        } else if (!binding.editTextPassword.text.toString()
                .equals(binding.editTextConfirmPassword.text.toString())
        ) {
            Toast.makeText(activity, getString(R.string.registration_wrong_confirmation),
                Toast.LENGTH_LONG
            ).show()
            return false
        }

        return true
    }

    private fun getUserData(): UserEntity {
        val user: UserEntity = UserEntity(
            binding.editTextEmailAddress.text.toString(),
            binding.editTextFullName.text.toString(),
            binding.editTextDate.text.toString(),
            binding.editTextGender.text.toString(),
            binding.editTextPassword.text.toString()
        )
        return user
    }

    private fun initDatePickerView() {

        val listener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }

        binding.editTextDate.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                listener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }

    private fun updateDateInView() {
        val myFormat = "dd/MM/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        binding.editTextDate.setText(sdf.format(cal.time))
    }

    private fun initGenderView() {
        binding.editTextGender.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                showDialog()
            }
        })
    }

    private fun showDialog() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.gender_dialog)

        val btn_male = dialog.findViewById(R.id.btn_male) as TextView
        btn_male.setOnClickListener {
            binding.editTextGender.setText(btn_male.text)
            dialog.dismiss()
        }
        val btn_female = dialog.findViewById(R.id.btn_female) as TextView
        btn_female.setOnClickListener {
            binding.editTextGender.setText(btn_female.text)
            dialog.dismiss()
        }
        dialog.show()
    }
}