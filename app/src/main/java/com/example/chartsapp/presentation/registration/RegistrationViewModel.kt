package com.example.chartsapp.presentation.registration

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.chartsapp.data.local.UserEntity
import com.example.chartsapp.domain.usecases.InsertUserDataInDbUseCase
import kotlinx.coroutines.launch

class RegistrationViewModel(private val insertUserInDbUseCase: InsertUserDataInDbUseCase) : ViewModel() {

    fun insertUserInDb(userEntity: UserEntity){
        viewModelScope.launch {
          insertUserInDbUseCase.execute(userEntity)
        }
    }
}