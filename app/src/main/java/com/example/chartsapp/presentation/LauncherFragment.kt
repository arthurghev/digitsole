package com.example.chartsapp.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.chartsapp.R
import com.example.chartsapp.common.AppPreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LauncherFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_launcher, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        GlobalScope.launch(context = Dispatchers.IO) {
            Thread.sleep(3000)
            if(AppPreferences.getToken() != null){
                findNavController().navigate(R.id.action_launcherFragment_to_myPageFragment)
            } else {
                findNavController().navigate(R.id.action_launcherFragment_to_loginFragment)
            }
        }
    }
}