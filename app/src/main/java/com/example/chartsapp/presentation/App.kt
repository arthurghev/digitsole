package com.example.chartsapp.presentation

import android.app.Application
import com.example.chartsapp.data.di.localDataSourceModule
import com.example.chartsapp.data.di.repositoryModule
import com.example.chartsapp.domain.di.domainModules
import com.example.chartsapp.common.AppPreferences
import com.example.chartsapp.presentation.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App: Application() {

    private val appModules = listOf(
        localDataSourceModule,
        repositoryModule,
        domainModules,
        viewModelModule
    )


    override fun onCreate() {
        super.onCreate()
        AppPreferences.init(this)

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(appModules)
        }
    }
}