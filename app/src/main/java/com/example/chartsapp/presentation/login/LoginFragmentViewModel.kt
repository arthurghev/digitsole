package com.example.chartsapp.presentation.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.chartsapp.common.utilities.ResourceUtil
import com.example.chartsapp.data.local.UserEntity
import com.example.chartsapp.domain.usecases.GetUserByEmailUseCase
import kotlinx.coroutines.launch

class LoginFragmentViewModel(private val getUserByEmailUseCase: GetUserByEmailUseCase) : ViewModel() {

    val userEntity = MutableLiveData<ResourceUtil<UserEntity>>()

    fun getUserByEmail(email: String){
        viewModelScope.launch {
            userEntity.postValue(ResourceUtil.loading(null))
            val data = getUserByEmailUseCase.execute(email).await()
            if (data != null) {
                userEntity.postValue(ResourceUtil.success(data))
            } else {
                userEntity.postValue(ResourceUtil.error("error",null))
            }
        }
    }

}