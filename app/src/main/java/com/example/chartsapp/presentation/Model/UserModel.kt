package com.example.chartsapp.presentation.Model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserModel(val email: String, val full_name: String, val age: String,
                     val gender: String, val password: String) : Parcelable

