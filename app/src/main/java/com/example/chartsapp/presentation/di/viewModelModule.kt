package com.example.chartsapp.presentation.di

import com.example.chartsapp.presentation.login.LoginFragmentViewModel
import com.example.chartsapp.presentation.registration.RegistrationViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { RegistrationViewModel(get()) }
    viewModel { LoginFragmentViewModel(get()) }
    //viewModel { otherViewModel(get()) }
}
