package com.example.chartsapp.domain.usecases

import com.example.chartsapp.data.local.UserEntity
import com.example.chartsapp.domain.repositories.UsersRepository
import kotlinx.coroutines.Deferred

class GetUserByEmailUseCase(private val repo: UsersRepository) {
    suspend fun execute(email: String) : Deferred<UserEntity?> {
        return repo.getUserByEmail(email)
    }
}
