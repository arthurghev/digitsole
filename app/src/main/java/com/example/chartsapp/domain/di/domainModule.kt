package com.example.chartsapp.domain.di

import com.example.chartsapp.domain.usecases.GetUserByEmailUseCase
import com.example.chartsapp.domain.usecases.InsertUserDataInDbUseCase
import org.koin.dsl.module


val domainModules = module {
    single { InsertUserDataInDbUseCase(get()) }
    single { GetUserByEmailUseCase(get()) }
    //single { otherUseCase(get()) }
}
