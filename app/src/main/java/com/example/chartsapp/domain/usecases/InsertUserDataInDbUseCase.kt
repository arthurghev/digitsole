package com.example.chartsapp.domain.usecases

import com.example.chartsapp.data.local.UserEntity
import com.example.chartsapp.domain.repositories.UsersRepository

class InsertUserDataInDbUseCase(private val repo: UsersRepository) {
    suspend fun execute(userEntity: UserEntity) {
        return repo.insertUserDataInDb(userEntity)
    }
}