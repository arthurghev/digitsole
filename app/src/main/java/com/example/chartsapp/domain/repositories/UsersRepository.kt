package com.example.chartsapp.domain.repositories

import com.example.chartsapp.data.local.UserEntity
import kotlinx.coroutines.Deferred

interface UsersRepository {
    suspend fun insertUserDataInDb(userEntity: UserEntity)
    suspend fun getUserByEmail(email: String) : Deferred<UserEntity?>
}