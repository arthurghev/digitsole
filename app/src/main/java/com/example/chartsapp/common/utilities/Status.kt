package com.example.chartsapp.common.utilities


enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
