package com.example.chartsapp.common.utilities

data class ResourceUtil<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {

        fun <T> success(data: T?): ResourceUtil<T> {
            return ResourceUtil(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): ResourceUtil<T> {
            return ResourceUtil(Status.ERROR, data, msg)
        }

        fun <T> loading(data: T?): ResourceUtil<T> {
            return ResourceUtil(Status.LOADING, data, null)
        }

    }
}
