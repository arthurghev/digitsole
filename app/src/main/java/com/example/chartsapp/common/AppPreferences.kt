package com.example.chartsapp.common

import android.content.Context
import android.content.SharedPreferences

object AppPreferences {
    private val NAME = "DigitSole"
    private val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences


    fun init(context: Context) {
        preferences = context.getSharedPreferences(NAME, MODE)
    }

    public fun setToken(token: String) {
        //preferences.edit().putString("Token", token)
        preferences.edit().putString("TOKEN", token).apply()
    }

    public fun getToken(): String? {
        return preferences.getString("Token", null)
    }

    public fun clearToken() {
        preferences.edit().remove("Token").apply()
    }
}