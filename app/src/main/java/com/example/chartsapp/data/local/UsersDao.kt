package com.example.chartsapp.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface UsersDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: UserEntity): Long

    @Query("SELECT * FROM users WHERE email= :email")
    fun getUserByEmail(email: String) : UserEntity?

    @Query("SELECT * FROM users WHERE gender LIKE :gender")
    fun findUsersByGender(gender: String): List<UserEntity>

    @Query("SELECT  COUNT(*) FROM users WHERE gender LIKE :gender")
    fun getUsersCountByGender(gender: String): Long

    @Query("SELECT  COUNT(*) FROM users")
    fun getUsersCount(): Long
}