package com.example.chartsapp.data.di

import com.example.chartsapp.data.local.UsersDb
import org.koin.dsl.module

val localDataSourceModule = module {
    factory { UsersDb.getInstance(get()) }
}
