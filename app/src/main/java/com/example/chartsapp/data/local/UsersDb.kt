package com.example.chartsapp.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = arrayOf(UserEntity::class),
    version = 1,
    exportSchema = false
)
abstract public class UsersDb : RoomDatabase() {
    abstract fun usersDao(): UsersDao

    companion object {

        private var INSTANCE: UsersDb? = null
        private val lock = Any()

        fun getInstance(context: Context): UsersDb {
            synchronized(lock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        UsersDb::class.java, "users.db"
                    ).build()
                }
                return INSTANCE!!
            }
        }
    }
}
