package com.example.chartsapp.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "users", indices = [Index(value = ["email"], unique = true)])
class UserEntity(
    @ColumnInfo(name = "email") var email: String, var full_name: String, var age: String,
    var gender: String, var password: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
}
