package com.example.chartsapp.data.di

import com.example.chartsapp.data.repositori.UsersRepositoryImp
import com.example.chartsapp.domain.repositories.UsersRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory<UsersRepository>{UsersRepositoryImp(get())}
}
