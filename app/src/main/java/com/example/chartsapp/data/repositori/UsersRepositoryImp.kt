package com.example.chartsapp.data.repositori

import com.example.chartsapp.data.local.UserEntity
import com.example.chartsapp.data.local.UsersDb
import com.example.chartsapp.domain.repositories.UsersRepository
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class UsersRepositoryImp(private val usersDB: UsersDb) : UsersRepository {
    override suspend fun insertUserDataInDb(userEntity: UserEntity) {
        withContext(Dispatchers.IO) {
            usersDB.usersDao().insert(userEntity)
        }
    }

    override suspend fun getUserByEmail(email: String): Deferred<UserEntity?>  {
        return withContext(Dispatchers.IO) {
            async {
                usersDB.usersDao().getUserByEmail(email)
            }
        }
    }

//    suspend fun getUsersByGender(email: String) : Deferred<List<UserEntity>> {
//        return withContext(Dispatchers.IO) {
//            async {
//                dao.findUsersByGender(email)
//            }
//        }
//    }
//
//    suspend fun getUsersCountByGender(gender: String) : Deferred<Long> {
//        return withContext(Dispatchers.IO) {
//            async {
//                dao.getUsersCountByGender(gender)
//            }
//        }
//    }
//
//    suspend fun getUsersCount() : Deferred<Long> {
//        return withContext(Dispatchers.IO) {
//            async {
//                dao.getUsersCount()
//            }
//        }
//    }
}